<?php require_once("logica-usuario.php");?>

<link href="bootstrap-3.3.7-dist/css/admin.css" rel="stylesheet">
<div class="container">
    <div class="login-container">
    <h1>Bem vindo!</h1>
    <?php
    if(usuarioEstaLogado()) {
    ?>
        <p class="text-success">Você está logado como <?= usuarioLogado() ?>. <a href="logout.php">Deslogar</a></p>
    <?php
    } else {
    ?>
            <div id="output"></div>
            <div class="avatar"></div>
            <div class="form-box">
                <form action="login.php" method="post">
                    <input name="user" type="text" placeholder="CPF" name="cpf">
                    <input type="password" placeholder="senha" name="senha">
                    <button class="btn btn-info btn-block login" type="submit">Login</button>
                </form>
            </div>
        </div>
        
</div>

<?php
}
?>